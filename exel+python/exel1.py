from openpyxl import Workbook

print("Podaj nazwę pliku: ")
file_name = input()

if len(file_name.strip()) == 0 or not file_name.isalnum():
    print("Podano nieprawidłową nazwę pliku")
    exit()

workbook = Workbook()
sheet = workbook.active

sheet["A1"] = "First name"
sheet["B1"] = "Last name"
sheet["C1"] = "Salary"

workbook.save(filename=file_name + ".xlsx")