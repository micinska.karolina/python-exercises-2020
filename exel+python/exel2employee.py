import os
import shutil

import openpyxl
from openpyxl import Workbook, load_workbook

from openpyxl.styles import Font, Color, Alignment, Border, Side, colors

from employee import Employee




def menu():
    print("""==========MENU==========
    1. Utwórz nowy plik (jeśli plik już istnieje to zostanie nadpisany)
    2. Wyświetl wszystkie rekordy
    3. Zaaktualizuj zarobki pracownika
    4. Dodaj nowego pracownika - na początku tabeli
    5. Dodaj nowego pracownika - na dole tabeli
    6. Usuń pracownika""")

    choice = int(input())
    if choice == 1:
        create_sheet()
    elif choice == 2:
        show_records()
    elif choice == 3:
        update_salary()
    elif choice == 4:
        add_new_employee()
    elif choice == 5:
        add_new_employee_v2()
    elif choice == 6:
        delete_employee()
    else:
        print("Podano niewłaściwy wybór")


def show_records():
    workbook = load_workbook(filename="out/123.xlsx")
    sheet = workbook.active

    # for i in range (1, 10):
    #     val = sheet["A" + str(i)].value
    #     if not val == None:
    #         print(val)

    lista = ["A", "B", "C", "D"]

    # for i in range (0, 3):
    #     val = sheet[lista[i] + "1"].value
    #     if not val == None:
    #         print(val)


    total = 0
    for value in sheet.iter_rows(min_row=2,
                                 max_row=10,
                                 min_col=1,
                                 max_col=5,
                                 values_only=True):

        if not value[0] == None:
            first_name = value[0]
            last_name = value[1]
            salary = value[2]
            total += int(salary)

            print("Employee")
            print(f"First name: {first_name}")
            print(f"Last name: {last_name}")
            print(f"Salary: {salary}\n")


    print(f"Total salary: {total}")
    sheet["E6"] = total
    print(total)
    print(sheet.max_row)

    workbook.save(filename="out/123.xlsx")


def create_sheet():

    print("Podaj nazwę pliku: ")
    file_name = input()

    if len(file_name.strip()) == 0 or not file_name.isalnum():
        print("Podano nieprawidłową nazwę pliku")
        exit()

    employee_list = []
    finish = False

    while (finish == False):
        print("Podaj imię pracownika: ")
        first_name = input()
        print("Podaj nazwisko pracownika: ")
        last_name = input()
        print("Podaj zarobki pracownika: ")

        try:
            salary = int(input())
        except:
            print("Podano niewłaściwą kwotę")
            continue

        employee = Employee(first_name, last_name, salary)
        employee_list.append(employee)

        print("Czy chcesz dodać kolejnego pracownika? Wpisz tak lub nie (t lub n)")

        choice = input()
        if choice.strip()[0].lower() == "n":
            break
        elif choice.strip()[0].lower() == "t":
            continue

    # employee1 = Employee("Stefan", "Dzik", 5000)
    # employee2 = Employee("Julita", "Kapusta", 6000)
    # employee3 = Employee("Marcelina", "Kura", 3000)
    #
    # employee_list = [employee1, employee2, employee3]

    workbook = Workbook()
    sheet = workbook.active


    sheet["A1"] = "First name"
    my_pink = openpyxl.styles.colors.Color(rgb='fc6ecd')
    my_fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_pink)
    # bold_font = Font(bold=True)
    # center_aligned_text = Alignment(horizontal="center")


    sheet["B1"] = "Last name"
    sheet["C1"] = "Salary"
    sheet["E1"] = "Total salary"

    total = 0
    i = 2
    for employee in employee_list:
        sheet["A" + str(i)] = employee.first_name
        sheet["B" + str(i)] = employee.last_name
        sheet["C" + str(i)] = employee.salary
        i += 1
        total += int(employee.salary)
        sheet["E2"] = total


    shutil.rmtree("out", ignore_errors=True)
    os.mkdir("out")

    workbook.save(filename="out/" + file_name + ".xlsx")

    menu()


def update_salary():
    workbook = load_workbook(filename="out/123.xlsx")
    sheet = workbook.active

    first_name = input("Podaj imię: ")
    last_name = input("Podaj nazwisko: ")
    new_salary = int(input("Podaj nowe zarobki: "))


    counter = 2
    for value in sheet.iter_rows(min_row=2,
                                 max_row=10,
                                 min_col=1,
                                 max_col=5,
                                 values_only=True):

        if not value[0] == None:
            if value[0] == first_name and value[1] == last_name:
                sheet["C" + str(counter)] = new_salary
                break

        counter += 1
    workbook.save(filename="out/123.xlsx")

    menu()

def add_new_employee():
    workbook = load_workbook(filename="out/123.xlsx")

    first_name = input("Podaj imię: ")
    last_name = input("Podaj nazwisko: ")
    salary = int(input("Podaj zarobki: "))

    sheet = workbook.active

    sheet.insert_rows(idx=2, amount=1)
    sheet["A2"] = first_name
    sheet["B2"] = last_name
    sheet["C2"] = salary


    workbook.save(filename="out/123.xlsx")

    menu()

def add_new_employee_v2():
    workbook = load_workbook(filename="out/123.xlsx")
    sheet = workbook.active

    first_name = input("Podaj imię: ")
    last_name = input("Podaj nazwisko: ")
    salary = int(input("Podaj zarobki: "))

    for cell in sheet.iter_rows(min_row=2,
                                max_row=sheet.max_row + 1,
                                min_col=1,
                                max_col=3):

        # print(cell[0].value)

        if cell[0].value == None:
            row = cell[0].row
            sheet["A" + str(row)] = first_name
            sheet["B" + str(row)] = last_name
            sheet["C" + str(row)] = salary
            break

    workbook.save(filename="out/123.xlsx")

    menu()

def delete_employee():
    workbook = load_workbook(filename="out/123.xlsx")
    sheet = workbook.active

    first_name = input("Podaj imię: ")
    last_name = input("Podaj nazwisko: ")

    for cell in sheet.iter_rows(min_row=2,
                                max_row=sheet.max_row,
                                min_col=1,
                                max_col=3):

        # print(cell[0].value)


        if cell[0].value == first_name and cell[1].value == last_name:
            row = cell[0].row
            sheet.delete_rows(idx=row)
            print("Usunięto rekord numer: " +str(row))
            workbook.save(filename="out/123.xlsx")
            menu()

    print("Nie znaleziono")
    menu()




menu()