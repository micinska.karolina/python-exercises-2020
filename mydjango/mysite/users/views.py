import json
from django.http import HttpResponse

from users.models import User, Account, Transaction
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods


@csrf_exempt
@require_http_methods("GET")
def get_users(request):
    users = User.objects.all()
    print(list(users))

    user_list = list(users)

    if len(user_list) == 0:
        return HttpResponse("Nie ma użytkowników", status=404)

    data = serializers.serialize('json', users)
    return HttpResponse(data, status=200)

    # return HttpResponse("Hello world!")
    # return HttpResponse("<html><body><h1>Mój html</h1><p>Mój paragraf</p></body></html>")


@csrf_exempt
@require_http_methods("POST")
def create_user(request):
    decoded_body = request.body.decode("utf-8")
    somejson = json.loads(decoded_body)
    first_name = somejson["first_name"]
    last_name = somejson["last_name"]
    age = int(somejson["age"])
    email = somejson["email"]
    pesel = somejson["pesel"]
    password = somejson["password"]

    user = User(email=email, first_name=first_name, last_name=last_name, age=age, pesel=pesel, password=password)
    user.save()

    return HttpResponse("Create user", status=201)

@csrf_exempt
@require_http_methods("GET")
def get_user_by_id(request, id):

    users = User.objects.filter(id=id)

    if len(list(users)) == 0:
        return HttpResponse("User not found", status=404)

    # user = users[0]
    # data = serializers.serialize('json', users)
    #
    # return HttpResponse(data, status=200)

    user = users[0]

    response = [{
        "id": user.id,
        "email": user.email,
        "age": user.age,
        "firstName": user.first_name
    }]
    return HttpResponse(response, status=200, content_type=json)

# wyłączenie odporności na atak cross site request forgery
@csrf_exempt
@require_http_methods("PATCH")
def activate(request, id):

    users = User.objects.filter(id=id)

    user_list = list(users)

    if len(list(users)) == 0:
        return HttpResponse ("Nie można aktywować użytkownika. Użytkownik nie istnieje", status=404)

    user = user_list[0]

    if user.active == True:
        return HttpResponse (f"Użytkownik {user.first_name} jest już aktywny", status=409)
    else:
        user.active = True
        user.save()
        return HttpResponse (f"Użytkownik {user.first_name} został aktywowany", status=200)

@csrf_exempt
@require_http_methods("PATCH")
def deactivate (request, id):

    users = User.objects.filter(id=id)

    user_list = list(users)

    if len(list(users)) == 0:
        return HttpResponse ("Nie można dezaktywować użytkownika. Użytkownik nie istnieje", status=404)

    user = user_list[0]

    if user.active == False:
        # lub if not user.active == True
        return HttpResponse (f"Użytkownik {user.first_name} już nie jest aktywny", status=409)
    else:
        user.active = False
        user.save()
        return HttpResponse (f"Użytkownik {user.first_name} został dezaktywowany", status=200)

@csrf_exempt
@require_http_methods("PATCH")
def reset_password(request, id):
    decode = request.body.decode("UTF-8")
    json_body = json.loads(decode)

    new_password = json_body["newPassword"]
    old_password = json_body["oldPassword"]

    user_query_set = User.objects.filter(id=id)
    user_list = list(user_query_set)

    if len(user_list) == 0:
        return HttpResponse("Nie znaleziono użytkownika", status=404)

    user = user_list[0]

    if not user.password == old_password:
        return HttpResponse("Podane hasło jest nieprawidłowe", status=409)

    if old_password == new_password:
        return HttpResponse("Podane hasła są takie same", status=409)

    user.password = new_password
    user.save()
    return HttpResponse("Hasło zostało zmienione", status=200)


@csrf_exempt
@require_http_methods("POST")
def create_account(request):
    decode = request.body.decode("UTF-8")
    json_body = json.loads(decode)

    user_id = json_body["userId"]

    user_query_set = User.objects.filter(id=user_id)
    user_list = list(user_query_set)

    if len(user_list) == 0:
        return HttpResponse("Nie ma takiego użytkownika", status=404)

    account = Account()
    account.user_id = user_id
    account.save()

    return HttpResponse("Utworzono konto", status=201)

@csrf_exempt
@require_http_methods("POST")
def transfer(request):
    decode = request.body.decode("UTF-8")
    json_body = json.loads(decode)

    sender_acc_no = json_body["senderAccNo"]
    receiver_acc_no = json_body["receiverAccNo"]
    amount = json_body["amount"]

    sender_account_query_set = Account.objects.filter(acc_no=sender_acc_no)
    sender_acc_list = list(sender_account_query_set)

    if len(sender_acc_list) == 0:
        return HttpResponse("Numer rachunku wysyłającego jest nieprawidłowy", status=404)

    receiver_acc_query_set = Account.objects.filter(acc_no=receiver_acc_no)
    receiver_acc_list = list(receiver_acc_query_set)

    if len(receiver_acc_list) == 0:
        return HttpResponse("Numer rachunku odbiorcy jest nieprawidłowy", status=404)

    sender = sender_acc_list[0]
    receiver = receiver_acc_list[0]

    sender.balance -= amount
    sender.save()

    receiver.balance += amount
    receiver.save()

    return HttpResponse("Przelew środków został zrealizowany", status=200)


    sender.balance -= amount
    sender.save()

    receiver.balance += amount
    receiver.save()

    transaction = Transaction()

    transaction.amount = amount
    transaction.receiver_account_no = receiver_acc_no
    transaction.sender_account_no = sender_acc_no
    transaction.type = "transfer"
    transaction.save()

    return HttpResponse("Przelew środków został zrealizowany", status=200)


@csrf_exempt
@require_http_methods("POST")
def deposit(request):
    decode = request.body.decode("UTF-8")
    json_body = json.loads(decode)

    accountNumber = json_body["accountNO"]
    amount = json_body["amount"]

    account_query_set = Account.objects.filter(acc_no=accountNumber)
    acc_list = list(account_query_set)

    if len(acc_list) == 0:
        return HttpResponse("Numer rachunku do wpłacenia jest nieprawidłowy", status=404)

    account = acc_list[0]

    account.balance += amount
    account.save()

    transaction = Transaction()
    transaction.type = "deposit"
    transaction.amount = amount
    transaction.receiver_account_no = accountNumber
    transaction.save()

    return HttpResponse("Wpłata środków została zrealizowana", status=200)

@csrf_exempt
@require_http_methods("POST")
def withdraw(request):
    decode = request.body.decode("UTF-8")
    json_body = json.loads(decode)

    accountNumber = json_body["accountNO"]
    amount = json_body["amount"]

    account_query_set = Account.objects.filter(acc_no=accountNumber)
    acc_list = list(account_query_set)

    if len(acc_list) == 0:
        return HttpResponse("Numer rachunku jest nieprawidłowy", status=404)

    account = acc_list[0]

    account.balance -= amount
    account.save()

    transaction = Transaction()
    transaction.sender_account_no = accountNumber
    transaction.amount = amount
    transaction.type = "withdraw"
    transaction.save()

    return HttpResponse("Wypłata środków została zrealizowana. Obecne salto to: " + str(amount), status=200)



@csrf_exempt
@require_http_methods("POST")
def login(request):
    decode = request.body.decode("UTF-8")
    json_body = json.loads(decode)

    email = json_body["email"]
    password = json_body["password"]

    user_queryset = User.objects.filter(email=email, password=password)

    user_list = list(user_queryset)

    if len(user_list) == 0:
        return HttpResponse("Nie znaleziono użytkownika", status=404)

    user = user_list[0]

    response = [{
             "id": user.id,
             "email": user.email,
             "age": user.age,
             "first_name": user.first_name,
             "last_name": user.last_name
         }]

    return HttpResponse (response, status=200, content_type=json)



@csrf_exempt
@require_http_methods("GET")
def get_user_account(request, id):

    account_gueryset = Account.objects.filter(user_id=id)

    account_list = list(account_gueryset)

    if len(account_list) == 0:
        return HttpResponse("Użytkownik nie posiada konta", status=404)

    data = serializers.serialize('json', account_list)

    return HttpResponse(data, status=200, content_type=json)





