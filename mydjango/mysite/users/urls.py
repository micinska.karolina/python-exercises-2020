from django.urls import path

from . import views

urlpatterns = [
    path('', views.get_users),
    path('create/', views.create_user),
    path('get/<int:id>', views.get_user_by_id),
    path('activate/<int:id>', views.activate),
    path('deactivate/<int:id>', views.deactivate),
    path('reset-password/<int:id>', views.reset_password),
    path('account/create', views.create_account),
    path('transfer', views.transfer),
    path('deposit', views.deposit),
    path('withdraw', views.withdraw),
    path('login', views.login),
    path('account/<int:id>', views.get_user_account)

]