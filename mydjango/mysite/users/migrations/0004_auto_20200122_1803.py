# Generated by Django 3.0.2 on 2020-01-22 17:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_users_pesel'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Users',
            new_name='User',
        ),
    ]
