import uuid

from django.db import models

class User(models.Model):
    # tworzenie zmiennej typu integer będącej kluczem prywatnym
    id = models.IntegerField(primary_key=True, auto_created=True)

    email = models.CharField(max_length=100, unique=True)

    first_name = models.CharField(max_length=100)

    last_name = models.CharField(max_length=100)

    password = models.CharField(max_length=500, null=True)

    age = models.IntegerField()

    pesel = models.CharField(max_length=100, null=True)

    active = models.BooleanField(default=False)

    password = models.CharField(null=True, max_length=100)

    # f to formatowanie stringa, wstrzykiwanie wartości
    def __str__(self):
        return f"id: {self.id}, " \
               f"email: {self.email}, " \
               f"firstname: {self.first_name}, " \
               f"lastname: {self.last_name}, " \
               f"age {self.age}, " \
               f"active: {self.active}"


class Account(models.Model):
    # balance = models.FloatField
    # acc_no = models.CharField(max_length=256, default=uuid.uuid4)
    acc_no = models.UUIDField(default=uuid.uuid4)

    balance = models.FloatField(default=0)

    user_id = models.IntegerField()

    active = models.BooleanField(default=False)


class Transaction(models.Model):
    sender_account_no = models.CharField(max_length=255)

    receiver_account_no = models.CharField(max_length=255)

    amount = models.FloatField()

    date = models.DateTimeField(auto_now=True)

    type = models.CharField(max_length=255, null=True)

