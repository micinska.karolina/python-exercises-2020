import http.client
import json

global_id = 0
global_first_name = ""
global_last_name = ""
global_age = 0
global_email = ""


def get_user_by_id_using_get(id):
    conn = http.client.HTTPConnection("127.0.0.1", "8000")

    headers = {
        'Content-Type': "application/json"
    }

    conn.request("GET", "/users/get/" + str(id), None, headers)

    res = conn.getresponse()
    print(f"Status code: {res.code}")

    if res.code == 404:
        print("Nie znaleziono użytkownika")
        menu()

    data = res.read()

    decoded = data.decode("utf-8")

    return decoded


def get_user_by_id():
    print("Wprowadź id użytkownika: ")

    id = 0
    try:
        id = int(input().strip())
    except:
        print("Wprowadzono teskt zamiast liczby")
        get_user_by_id()

    response = get_user_by_id_using_get(id).replace("\'", "\"")
    json_body = json.loads(response)

    user_id = json_body["id"]
    email = json_body["email"]
    age = json_body["age"]
    first_name = json_body["firstName"]

    print(f"id: {user_id}")
    print(f"email: {email}")
    print(f"age: {age}")
    print(f"first name: {first_name}")
    menu()


def login():
    global global_id
    global global_first_name
    global global_last_name
    global global_email
    global global_age

    email = input("Wprowadź email: \n")
    password = input("Wprowadź hasło: \n")

    conn = http.client.HTTPConnection("localhost:8000")

    headers = {
        'Content-Type': 'application/json'
    }

    payload = {
        "email": email,
        "password": password
    }

    str_payload = str(payload).replace("\'", "\"")

    conn.request("POST", "/users/login", str_payload, headers)

    res = conn.getresponse()
    data = res.read()

    decode = data.decode("utf-8").replace("\'", "\"")

    json_body = json.loads(decode)
    id = json_body["id"]
    email = json_body["email"]
    first_name = json_body["first_name"]
    last_name = json_body["last_name"]
    age = json_body["age"]

    global_id = id
    global_first_name = first_name
    global_last_name = last_name
    global_age = age
    global_email = email

    print(f"Witaj, {first_name}")

    submenu()




def menu():
    print("""
==========================MENU===========================
1. Pobierz użytkownika po ID
2. Logowanie
3. Zakończ
""")
    choice = 0
    try:
        choice = int(input("Wpisz 1, 2 lub 3: \n").strip())
    except:
        print("Podana wartość nie jest liczbą")
        menu()

    if choice == 1:
        get_user_by_id()
    elif choice == 2:
        login()
    elif choice == 3:
        exit(0)
    else:
        print("Wybór nieprawidłowy")
        menu()


def check_balance():
    global global_id

    conn = http.client.HTTPConnection("127.0.0.1", "8000")

    headers = {
        'Content-Type': "application/json"
    }

    conn.request("GET", "/users/account/" + str(global_id), None, headers)

    res = conn.getresponse()
    if res.code == 404:
        print("Nie znaleziono konta")
        submenu()

    data = res.read().decode("utf-8")

    accounts = json.loads(data)

    total_amount = 0

    for account in accounts:
        fields = account["fields"]
        acc_no = fields["acc_no"]
        balance = fields["balance"]
        print(f"Numer konta: {acc_no}")
        print(f"Stan konta: {balance}")

        total_amount += balance
        print(f"Suma wszystkich sald: {total_amount}")



    submenu()





def submenu():
    print("==========SUBMENU===========")
    print("1. Sprawdź stan konta")
    print("2. Wyloguj się")

    choice = 0

    # zaznaczyć choice -> dwa razy shift -> surround with -> try/except:
    try:
        choice = int(input())
    except ValueError:
        print("Value error")
        submenu()
    except:
        print("Some error")
        submenu()



    if choice == 1:
        check_balance()
    elif choice == 2:
        print("Wylogowano")
        menu()

    else:
        print("Nieprawidłowy wybór")
        submenu()


menu()